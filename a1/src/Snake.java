import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.Timer;

/**
 * Created by Administrator on 2016/5/14.
 */


public class Snake {
    static int NORTH = 1;
    static int SOUTH = -1;
    static int WEST = 2;
    static int EAST = -2;
    static int SQUARESIZE = 10;
    static int SPEED = 5;
    static int FPS = 30;

    int score;
    private JFrame gameFrame;
    private SnakeShow body;


    // inner classsssssssssss-------------------------
     static class SnakeShow extends JPanel {
        private int direction;
        private int tempDirection;
        LinkedList<Rectangle> squares;
        Rectangle fruit;
        LinkedList<Rectangle> walls;
        int level;

        SnakeShow() {
            init();

         };
        void init() {
            squares = new LinkedList<>();
            walls = new LinkedList<>();
            direction = 0;
            tempDirection = 0;

            for (int i = 0;i<15 ;i++) {
                walls.push(new Rectangle(400,i*10,SQUARESIZE,SQUARESIZE));
            }
            for (int i = 1;i<=15 ;i++) {
                walls.push(new Rectangle(200,400-i*10,SQUARESIZE,SQUARESIZE));
            }
            for (int i = 0;i<28 ;i++) {
                walls.push(new Rectangle(i*10,150,SQUARESIZE,SQUARESIZE));
            }
            for (int i = 1;i<=28 ;i++) {
                walls.push(new Rectangle(600-i*10,250,SQUARESIZE,SQUARESIZE));
            }

            //initiate a random location for the snake
            Random randomgen = new Random();
            int randomx = (3+randomgen.nextInt(73))*10;
            int randomy = (3+randomgen.nextInt(53))*10;
            while (true) {
                randomx = randomgen.nextInt(60)*10;
                randomy = randomgen.nextInt(40)*10;

                Rectangle snakeTemp = new Rectangle(randomx,randomy,SQUARESIZE,SQUARESIZE);
                if(!walls.contains(snakeTemp)) {
                    break;
                }

            }
            Rectangle firstS = new Rectangle(randomx,randomy,SQUARESIZE,SQUARESIZE);
            squares.add(firstS);
        }
        // generate the fruit
        void fruitGen() {
            Random randomgen = new Random();
            int randomx = (3+randomgen.nextInt(73))*10;
            int randomy = (3+randomgen.nextInt(53))*10;
            while (true) {
                randomx = randomgen.nextInt(60)*10;
                randomy = randomgen.nextInt(40)*10;
                Rectangle fruitTemp = new Rectangle(randomx,randomy,SQUARESIZE,SQUARESIZE);
                if((!squares.contains(fruitTemp)) && (!walls.contains(fruitTemp))) {
                    break;
                }


            }

            fruit = new Rectangle(randomx,randomy,SQUARESIZE,SQUARESIZE);
        }

        //check if the fruit is eaten
        boolean eat() {
            return squares.getFirst().equals(fruit);
        }


        //check the direction to add the square
         Rectangle check_direction(Rectangle square, int direction) {
             Rectangle square2 = new Rectangle(100,100,SQUARESIZE,SQUARESIZE);

             if (direction == NORTH) {
                 square2.setLocation(square.x,((square.y-10)+400)%400);
             } else if (direction == EAST) {
                 square2.setLocation((square.x+10)%600,square.y);
             } else if (direction == WEST) {
                 square2.setLocation(((square.x-10)+600)%600,square.y);
             } else if (direction == SOUTH) {
                 square2.setLocation(square.x,(square.y+10)%400);
             }

             return  square2;
         }




         //TODO add position later
          void addLength() {
              Rectangle last = squares.getLast();
              //get the last direction
              int addDirection = 0;
              if(squares.size() == 1) {
                  addDirection = -direction;
              } else {
                  Rectangle secondLast = squares.get(squares.size()-2);
                  if(secondLast.y > last.y) {
                      addDirection = NORTH;
                  } else if(secondLast.y < last.y) {
                      addDirection = SOUTH;
                  } else if(secondLast.x > last.x) {
                      addDirection = WEST;
                  } else if(secondLast.x < last.x) {
                      addDirection = EAST;
                  }
              }
              if(addDirection == 0) {System.out.print("add direction = 0");}    //check, should never happen

              Rectangle addSquare = check_direction(last,addDirection);
              squares.addLast(addSquare);

         }

        //paint
         @Override
         protected void paintComponent(Graphics g) {
             super.paintComponent(g);
             g.drawRect(100,75,600,400);

             //paint the snake body
             for(int i=0;i<squares.size();i++) {
                 Rectangle sq = squares.get(i);
                 g.setColor(Color.green);
                 if(isDead() && sq.equals(squares.getFirst())) {g.setColor(Color.red);}
                 g.fillRect(sq.x+100,sq.y+75,sq.width,sq.height);
             }
             for(int i=0;i<walls.size();i++) {
                 Rectangle sq = walls.get(i);
                 g.setColor(Color.gray);
                 if(isDead() && sq.equals(squares.getFirst())) {g.setColor(Color.red);}
                 g.fillRect(sq.x+100,sq.y+75,sq.width,sq.height);
             }

             //paint the fruit
             g.setColor(Color.YELLOW);
             g.fillRect(fruit.x+100,fruit.y+75,fruit.width,fruit.height);

         }

         // move the snake, with the head moving to the direction, others move
         // to the sqaure in front of it
         public void move() {
             //the first square get moving
            if(direction == 0) {return;}
             Rectangle firstSquare = squares.getFirst();
                        //System.out.println(direction);
             Rectangle newFirst = check_direction(firstSquare,direction);
             squares.addFirst(newFirst);
             squares.removeLast();

         }

        //check death
        boolean isDead() {
            Rectangle firstSquare = squares.getFirst();
            for(int i=1;i<squares.size();i++) {
                Rectangle sq = squares.get(i);
                if(sq.equals(firstSquare)) {return true;}
            }
            if(walls.contains(firstSquare)) {return true;}

            return false;
        }


         //set direction
         void setDirection() {
             if(this.direction == -tempDirection) { //cannot change to backward direction
                 return;
             }
             this.direction = tempDirection;
         }

         void setTempDirection(int direction) {
            if(this.direction == -direction) { //cannot change to backward direction

                return;
            }
            this.tempDirection = direction;
        }
        int getDirection() {
            return direction;
        }

    }

    //-----------------------------------------
    Snake() {
        gameFrame = new JFrame("snake");
        gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameFrame.setResizable(false);
        gameFrame.setSize(800,600);


    }

    JFrame getgameFrame() {
        return gameFrame;
    }



    void init() {

        JPanel cover = new JPanel();
        cover.setSize(800,700);
        cover.setLayout(new BorderLayout());

        //main
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
       // mainPanel.setSize(800,500);


        //mainPanel.setLocation(0,0);
        //button
        JPanel startButtonP = new JPanel();
        startButtonP.setSize(200,50);
        JButton startButton = new JButton("start");
        startButton.setPreferredSize(new Dimension(200,50));
        startButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // super.mouseClicked(e);
                //frame.add(gamePanel);
                cover.setVisible(false);
                gameFrame.repaint();
                run();
                //   System.out.println(gamePanel);
            }
        });
        startButtonP.add(startButton);

        //label the name snake
        JPanel gameLabelPanel = new JPanel();
        gameLabelPanel.setSize(400,400);
        gameLabelPanel.setLayout(new GridLayout());

        JLabel gameName = new JLabel("<html><center><font size=\"6\">Snake</font>"
                +"<center> <font size=\"3\">Yuyang Zhu   y89zhu</font>"
                +"<p></p><center>East the fruit as the snake"
                + "<center>Die when you eat yourself or hit a wall"
                +"<p></p><center>Use ARROW KEYS to start and move the snake"
                + "<center>Press SPACE to pause"
                + "<center>Press ENTER to rest</html>",SwingConstants.CENTER);
        gameName.setPreferredSize(new Dimension(500,200));
        gameLabelPanel.add(gameName);
        mainPanel.add(gameName,BorderLayout.CENTER);

        //

        //mainPanel.add(disP2);


        mainPanel.add(startButtonP,BorderLayout.SOUTH);
        cover.add(mainPanel,BorderLayout.PAGE_START);
        gameFrame.add(cover);





        body = new SnakeShow();

        body.fruitGen();
;
        gameFrame.getContentPane().add(body);
        body.setFocusable(true);
        body.requestFocusInWindow();
        gameFrame.repaint();

    }



    void run() {
        Timer freshTimer = new Timer();
        freshTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                gameFrame.repaint();
            }
        }, (1000/FPS),(1000/FPS));
        score = 0;
        String deadMessage = "   ";
        Label scoreL = new Label("SCORE: " + Integer.toString(score));
        scoreL.setPreferredSize(new Dimension(300,50));

        body.add(scoreL,BorderLayout.NORTH);

        class DirectionListener extends KeyAdapter {
            public void keyPressed(KeyEvent e) {
                int old = body.getDirection();
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                    body.setTempDirection(NORTH);
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    body.setTempDirection(EAST);
                } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    body.setTempDirection(WEST);
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    body.setTempDirection(SOUTH);
                }
                //body.setDirection();
                //if (old != body.getDirection()) {body.move();}
                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    body.setTempDirection(0);
                }
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                   // gameFrame.getContentPane().remove(body);gameFrame.getContentPane().add(body);

                    body.init();
                    body.fruitGen();
                    score=0;
                    scoreL.setText("SCORE: " + Integer.toString(score));
                }

            }
        }
        body.addKeyListener(new DirectionListener());


        Timer moveTimer = new Timer();
        moveTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //make the snake run
                body.setDirection();
                body.move();
                if(body.isDead()) {
                    moveTimer.cancel();
                    scoreL.setText("SCORE: " + Integer.toString(score) + "   You are dead! Press ENTER to restart");
                }
                if (body.eat()) {
                    body.addLength();
                    body.fruitGen();
                    score++;
                    scoreL.setText("SCORE: " + Integer.toString(score));

                }

                //need to generate fruit and check if the fruit is eaten


            }
        }, 1000/SPEED,1000/SPEED);



    } //end run


    public void setSPEED(int speed) {
        SPEED = speed;
    }
    public void setFPS(int fps) {
        FPS = fps;
    }

}
