import java.awt.*;

/**
 * Created by Administrator on 2016/6/17.
 */
interface IPanelView {
    public void updateView();
}

public class PanelModel {
    private IPanelView view;
    int width,height;

    int toolpick;
    int colorpick;
    int thickpick;
    int thickpickI;
    Color fillColor;





    PanelModel() {

    }

    void setView(IPanelView view) {
        this.view = view;

    }

    void setWidth(int width) {
        this.width = width;
    }
    void setHight(int hight) {
        this.height = hight;
    }

    void setToolpick(int i) {
        toolpick = i;
        view.updateView();
    }
    void setColorpick(int i,Color color) {
        colorpick = i;
        fillColor = color;
        view.updateView();
    }
    void setThickpick(int i) {
        thickpick = i;
        view.updateView();
    }


}
