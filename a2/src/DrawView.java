import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Administrator on 2016/6/18.
 */
public class DrawView extends JPanel implements IDrawView{

    DrawModel model;
    Rectangle drawArea;



    DrawView(DrawModel model) {
        this.model = model;
        drawArea = new Rectangle(model.x,model.y,model.width,model.height);
        //mouse clicked and such
        MouseListener a = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                if (drawArea.contains(e.getX(),e.getY())) {
                    Point a = new Point(e.getX(),e.getY());      //   System.out.println(a);
                    model.setMousePress(a);
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {

                super.mouseDragged(e);
                System.out.println("mouse Dragged");
                if (drawArea.contains(e.getX(),e.getY())) {
                    Point a = new Point(e.getX(),e.getY());
                    model.setMouseDrag(a);
                }

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                model.setMouseRelease();
            }
        };
        this.addMouseListener(a);
        // mouse dragged
        MouseMotionAdapter aa = new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
               // System.out.println("mouse Dragged");
                if (drawArea.contains(e.getX(),e.getY())) {
                    Point a = new Point(e.getX(),e.getY());
                    model.setMouseDrag(a);
                }

            }
        };
        this.addMouseMotionListener(aa);

    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //draw a general rectangle
        g.setColor(Color.white);
      //   g.fillRect(model.x,model.y,model.width,model.height);  //model.width,model.height
        g.fillRect(this.getX(),this.getY(),this.getWidth(),this.getHeight());



        for (int i=0;i<model.shapes.size();i++) {

            Graphics2D g2 = (Graphics2D)g.create();

            Shape s = model.shapes.get(i);
            g2.setStroke(new BasicStroke(s.thick));
            if(s.color != null) {
                g2.setColor(s.color);
            }

                                                           //  System.out.println(s.p.y);
            String type = s.type;
            if(type.equals("rect")) {
                if(s.color != null) {
                    g2.fillRect(s.p.x,s.p.y,s.width,s.height);
                }
                g2.setColor(Color.black);
                g2.drawRect(s.p.x,s.p.y,s.width,s.height);
            } else if (type.equals("circle")) {
                if(s.color != null) {
                    g2.fillOval(s.p.x,s.p.y,s.width,s.height);
                }
                g2.setColor(Color.black);
                g2.drawOval(s.p.x,s.p.y,s.width,s.height);
            } else if (type.equals("line")) {
                g2.setColor(Color.black);
                s.p2.x = s.p.x + s.width;
                s.p2.y = s.p.y + s.height;
                g2.drawLine(s.p.x,s.p.y,s.p2.x,s.p2.y);
            }

        }



    }

    public void updateView() {
        drawArea = new Rectangle(this.getX(),this.getY(),this.getWidth(),this.getHeight());
        this.repaint();

    }
}
