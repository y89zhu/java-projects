import javafx.scene.control.ColorPicker;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/6/17.
 */
public class PanelView extends JPanel implements IPanelView{
    PanelModel model;
    JPanel toolPicker;
    JPanel colorPicker;
    JButton colorChooser;
    JPanel thickChooser;

    ArrayList<JButton> toolButtons;
    ArrayList<JButton> colorButtons;
    ArrayList<JButton> thickButtons;

    Border thickBorder = new LineBorder(Color.BLACK, 4);
    Border normalBorder = new LineBorder(Color.BLACK, 1);

    int thick1 = 1;
    int thick2 = 5;
    int thick3 = 10;
    int thick4 = 20;


    PanelView(PanelModel model) {
        //draw a general rectangle
        int buttonWidth1 = model.width/2;
        int buttonHeight1 = model.height/12;
        int buttonwidth2 = model.width;
        this.model = model;

        this.setBorder(normalBorder);

        this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
        toolButtons = new ArrayList<>();
        colorButtons = new ArrayList<>();
        thickButtons = new ArrayList<>();
        //GridBagConstraints move = new GridBagConstraints();

        //tool picker
        toolPicker = new JPanel();
        toolPicker.setBorder(normalBorder);
        toolPicker.setLayout(new GridLayout(3,2));
        //toolPicker.setSize(new Dimension(model.width-10,model.height/3-10));
        toolPicker.setPreferredSize(new Dimension(model.width-10,model.height/3-10));
        //buttons
        ActionListener toolListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


            }
        };
        JButton pickerButton = new JButton("picker");
        //pickerButton.setPreferredSize(new Dimension(model.width/2-10,model.height/12-10));
        pickerButton.setSize(new Dimension(model.width/2,model.height/12-10));
        pickerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setToolpick(0);
            }
        });
        JButton rectButton = new JButton("rect");
        rectButton.setPreferredSize(new Dimension(model.width/2-10,model.height/12-10));
        rectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setToolpick(1);
            }
        });
        JButton circleButton = new JButton("circle");
        JButton lineButton = new JButton("line");
        JButton eraserButton = new JButton("eraser");
        JButton fillButton = new JButton("fill");

        circleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setToolpick(2);
            }
        });
        lineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setToolpick(3);
            }
        });
        eraserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setToolpick(4);
            }
        });
        fillButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setToolpick(5);
            }
        });

        toolPicker.add(pickerButton);
        toolPicker.add(rectButton);
        toolPicker.add(circleButton);
        toolPicker.add(lineButton);
        toolPicker.add(eraserButton);
        toolPicker.add(fillButton);
        toolButtons.add(pickerButton);
        toolButtons.add(rectButton);
        toolButtons.add(circleButton);
        toolButtons.add(lineButton);
        toolButtons.add(eraserButton);
        toolButtons.add(fillButton);


        //color picker
        colorPicker = new JPanel();
        colorPicker.setLayout(new GridLayout(3,2));
        // toolPicker.setSize(new Dimension(model.width-10,model.hight/3-10));
        colorPicker.setPreferredSize(new Dimension(model.width-10,model.height/3-10));
        JButton black = new JButton("");
        black.setBackground(Color.black);
        black.setPreferredSize(new Dimension(buttonWidth1,buttonHeight1));
        JButton blue = new JButton("");
        blue.setBackground(Color.blue);
        JButton red = new JButton();
        red.setBackground(Color.red);
        JButton yellow = new JButton();
        yellow.setBackground(Color.yellow);
        JButton green = new JButton();
        green.setBackground(Color.green);

        //add buttons to the list
        colorButtons.add(black);
        colorButtons.add(blue);
        colorButtons.add(red);
        colorButtons.add(yellow);
        colorButtons.add(green);
        for(int i=0;i<colorButtons.size();i++) {
            colorPicker.add(colorButtons.get(i));
        }
        black.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setColorpick(0,Color.black);
            }
        });
//        black.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseClicked(MouseEvent e) {
//                super.mouseClicked(e);
//
//            }
//        });

        blue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setColorpick(1,Color.blue);
            }
        });
        red.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setColorpick(2,Color.red);
            }
        });
        yellow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setColorpick(3,Color.yellow);
            }
        });
        green.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.setColorpick(4,Color.green);
            }
        });


        //colorButtons.add(black);


        //color chooser
        JPanel chooserP = new JPanel();
        chooserP.setLayout(new GridLayout(2,1));
      //  chooserP.setLayout(new BorderLayout());
        colorChooser = new JButton("choose color");

        colorChooser.setPreferredSize(new Dimension(model.width/12-10,buttonHeight1));
        chooserP.add(colorChooser);
        JButton chosserDisplay = new JButton();
        chosserDisplay.setBackground(Color.white);
        colorButtons.add(chosserDisplay);
        chosserDisplay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color a = chosserDisplay.getBackground();model.setColorpick(5,a);
            }
        });
        chooserP.add(chosserDisplay);


        colorChooser.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                Color c = JColorChooser.showDialog(null, "Choose a Color",chosserDisplay.getBackground());
                if(c!= null) {
                    chosserDisplay.setBackground(c);
                    model.setColorpick(5,c);
                }
            }
        });

        //
        thickChooser = new JPanel();
        thickChooser.setLayout(new GridLayout(4,1));
        // toolPicker.setSize(new Dimension(model.width-10,model.hight/3-10));
        thickChooser.setPreferredSize(new Dimension(model.width-10,model.height/3-10));
        JButton line1 = new JButton("thick1px");

        line1.setPreferredSize(new Dimension(buttonwidth2,buttonHeight1));
        JButton line2 = new JButton("thick5px");
        JButton line3 = new JButton("thick10px");
        JButton line4 = new JButton("thick20px");

        thickButtons.add(line1);
        thickButtons.add(line2);
        thickButtons.add(line3);
        thickButtons.add(line4);

        for(int i=0;i<thickButtons.size();i++) {
            thickChooser.add(thickButtons.get(i));
        }
        line1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.thickpickI = 0;
                model.setThickpick(thick1);

            }
        });
        line2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.thickpickI = 1;
                model.setThickpick(thick2);

            }
        });
        line3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.thickpickI = 2;
                model.setThickpick(thick3);

            }
        });
        line4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.thickpickI = 3;
                model.setThickpick(thick4);

            }
        });





        //add to this panel
        this.add(toolPicker);
        this.add(colorPicker);
        this.add(chooserP);
        this.add(thickChooser);

        //this.add(pp);


      //  toolPicker.add(Box.createRigidArea(new Dimension(0,5)));
       // toolPicker.add(pickerButton);

        //this.add(pickerButton);

    }
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //draw a general rectangle
       // g.drawRect(10,10,model.width,model.hight);




    }



    public void updateView() {
        System.out.println("PanelView: updateView");
        //tool
        for(int i=0;i<toolButtons.size();i++) {
            toolButtons.get(i).setForeground(Color.black);
            Border thickBorder = new LineBorder(Color.BLACK, 3);
            toolButtons.get(i).setBorder(normalBorder);
        }
        if(model.toolpick >= 0) {
            toolButtons.get(model.toolpick).setForeground(Color.blue);

            toolButtons.get(model.toolpick).setBorder(thickBorder);
        }
        //color
        for(int i=0;i<colorButtons.size();i++) {
            colorButtons.get(i).setBorder(normalBorder);
        }
        if(model.toolpick >= 0) {
            colorButtons.get(model.colorpick).setBorder(thickBorder);
        }
        //thick
        for(int i=0;i<thickButtons.size();i++) {
            thickButtons.get(i).setBorder(normalBorder);
        }
        if(model.thickpickI >= 0) {
            thickButtons.get(model.thickpickI).setBorder(thickBorder);
        }
    }
}
