import javax.swing.text.Position;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016/6/18.
 */

interface IDrawView {
    public void updateView();
}
public class DrawModel {

    IDrawView view;
    PanelModel panelModel;
    //some default value;
    int width = 450;
    int height = 500;
    int x = 0;
    int y = 20;


    Shape selectShape;

    Point mousePress;
    Point mouseRelease;
    Point mouseDrag;
    Point mouseLastPressed;

    ArrayList<Rectangle> rectangles;
    ArrayList<Shape> shapes;
    Rectangle drawArea;
    //this class contains the required data to draw the shapes
//    class Shape {
//        String type;
//        Point p;
//        Point p2; //for line
//        int width; //for rec
//        int height;  //for rec
//        //int radius; //for circle
//        Color color;
//        int thick;
//       ;
//
//
//        Shape(Point p) {
//            this.p = p;
//            p2 = new Point(-100,-100);
//            width = 0;
//            height = 0;
//            thick = 0;
//            color = null;
//        }
//        String getDataString() {
//            String result = " ";
//            String colorString;
//            if(color == null) {
//                colorString = "0";
//            } else {
//                colorString = String.valueOf(color.getRGB());
//            }
//            result = result + type + " " + Integer.toString(p.x) + " " + Integer.toString(p.y) + " " + Integer.toString(p2.x) + " " + Integer.toString(p2.y) + " "
//                + Integer.toString(width) + " " + Integer.toString(height) + " "+ Integer.toString(p2.x) + " "  + colorString ;
//
//
//            //result = result + type + Integer.toString(p.x) + " " + p.y + " " + p2.x + " " + p2.y + " " + width + " " + height  + " " + thick + " ";
//            System.out.println(result);
//            return  result;
//        }
//
//    }

    DrawModel() {
        rectangles = new ArrayList<>();
        shapes = new ArrayList<>();
        drawArea = new Rectangle(x,y,width,height);
    }

    public void setMousePress(Point mousePress) {
        this.mousePress = mousePress;
        mouseLastPressed = mousePress;
        selectShape = new Shape(new Point(-1000,-1));

        // get the selected shape
        for (int i=shapes.size()-1;i>=0;i--) {
            Shape s = shapes.get(i);
            String type = s.type;
            if (type.equals("line")) {
                Line2D a = new Line2D.Double();
                //Double x = s.p.x;

                a.setLine(s.p.x,s.p.y,s.p2.x,s.p2.y);
                Point2D b = new Point2D.Double();
                b.setLocation(mousePress.x,mousePress.y);
                double dis = a.ptLineDist(b);
                if(dis < 5) {
                    selectShape = s;
                    break;
                }

            } else if (type.equals("circle")) {
                long radius = s.width/2;
                long x = radius+s.p.x;
                long y = radius+s.p.y;
//            long d2 = (mousePress.x - x)*(mousePress.x - x)+(mousePress.y - y)*(mousePress.y - y);
//            long d3 = (mousePress.x - x)^2+(mousePress.y - y)^2;
//            long c = radius*radius;
//                System.out.println(x + " " + y + " " + s.p.x);
//                System.out.println("circle pick check, distance "  + radius + "compared to " + c);

                if((mousePress.x - x)*(mousePress.x - x)+(mousePress.y - y)*(mousePress.y - y) <= radius*radius) {
                    selectShape = s;
                    break;
                }

            } else {
                Rectangle r = new Rectangle(s.p.x,s.p.y,s.width,s.height);
                if(r.contains(mousePress)) {
                    selectShape = s;
                    break;
                }
            }

        }

        if(panelModel.toolpick == 0) {  //picker
            if(selectShape.p.x != -1000) {

            }


        } else if (panelModel.toolpick == 1) {  //rect
            //create a rect, small though
            Shape s = new Shape(mousePress);
            s.type = "rect";
            shapes.add(s);
            s.thick = panelModel.thickpick;
            selectShape = s;

        } else if (panelModel.toolpick == 2) {
            Shape s = new Shape(mousePress);
            s.type = "circle";
            shapes.add(s);
            s.thick = panelModel.thickpick;
            selectShape = s;
        } else if (panelModel.toolpick == 3) {
            Shape s = new Shape(mousePress);
            s.type = "line";
            s.p2 = mousePress;
            shapes.add(s);
            s.thick = panelModel.thickpick;
            selectShape = s;
        } else if (panelModel.toolpick == 4) { //eraser
            if(selectShape.p.x != -1000) {
                shapes.remove(selectShape);
            }
        } else if (panelModel.toolpick == 5) { //fill
            if(selectShape.p.x != -1000) {
                selectShape.color = panelModel.fillColor;
                        System.out.println("fill color is " + panelModel.fillColor);
            }
        }

        //color pick used for fill
        if(panelModel.colorpick == 0) {
            //
        }

        //thinkess


        //notify view
        view.updateView();
    }

    public void setMouseDrag(Point mouseDrag) {

        this.mouseDrag = mouseDrag;

        this.mouseRelease = mouseDrag;

        Shape s = selectShape;  //must be the last shape anyway
        if(panelModel.toolpick == 0) {  //picker
            if(selectShape.p.x != -1000) {
                if(drawArea.contains(mouseDrag)) {
                    selectShape.p.x += mouseDrag.x-mouseLastPressed.x;
                    selectShape.p.y += mouseDrag.y-mouseLastPressed.y;



                }
            }

        } else if (panelModel.toolpick == 1) {  //rect

            int width = Math.max(mouseDrag.x-mousePress.x,1);
            int height = Math.max(mouseDrag.y-mousePress.y,1);
            s.width = width;
            s.height = height;
        } else if (panelModel.toolpick == 2) {  //circle

            int width = Math.max(mouseDrag.x-mousePress.x,1);
            int height = Math.max(mouseDrag.y-mousePress.y,1);
            s.width = width;
            s.height = width;

        } else if (panelModel.toolpick == 3) {  //circle

            s.p2 = mouseDrag;
            s.width = s.p2.x - s.p.x;
            s.height = s.p2.y - s.p.y;
        }
        //update last pressed
        mouseLastPressed = mouseDrag;
        //notify view
        view.updateView();

    }
    public  void setMouseRelease() {
        selectShape = new Shape(new Point(-1,-1));

    }
    public  void setShapes(ArrayList shapes) {
        this.shapes = shapes;
        view.updateView();
    }

    public void setPanelModel(PanelModel panelModel) {
        this.panelModel = panelModel;
    }

    public void setView(IDrawView view) {
        this.view = view;
        //drawArea = new Rectangle(view.getx,view.getY(),view.getWidth(),view.getHeight());
    }
}
