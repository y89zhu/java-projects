import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Administrator on 2016/6/17.
 */
public class SketchMain {

    public static void  main(String[] args) {
        JFrame frame = new JFrame("Sketch");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800,620);


        JPanel mainPanel = new JPanel();
        ;

        mainPanel.setLayout(new BorderLayout());

        JScrollPane mainPanelS = new JScrollPane();
       // mainPanelS.setBounds(5,5,795,615);
        mainPanelS.setBackground(Color.yellow);
        mainPanelS.setSize(800,620);
        mainPanelS.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        mainPanelS.add(mainPanel);


        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(1,1));
        contentPane.add(mainPanelS);
        contentPane.setSize(800,620);


        mainPanel.setSize(790,610);

        frame.getContentPane().add(contentPane);
        //mainPanelS.add(mainPanel);
//       tool  panel
        PanelModel panelModel = new PanelModel();
        PanelView panelView = new PanelView(panelModel);
        panelView.setPreferredSize(new Dimension(150,550));


        panelModel.setHight(500);
        panelModel.setWidth(150);
        panelModel.setView(panelView);

        JPanel panelViewWrapper = new JPanel();
        panelViewWrapper.add(panelView);
        //drawing panel
        DrawModel drawModel = new DrawModel();
        JPanel drawPanelOut = new JPanel();
        drawPanelOut.setPreferredSize(new Dimension(drawModel.width+10,drawModel.height+10));
        DrawView drawView = new DrawView(drawModel);
        //drawView.setSize(new Dimension(drawModel.width,drawModel.height));
        drawView.setPreferredSize(new Dimension(drawModel.width,drawModel.height));


        drawModel.setPanelModel(panelModel);
        drawModel.setView(drawView);


        drawPanelOut.add(drawView);





        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenu view = new JMenu("View");
        menuBar.add(file);
        JMenuItem save = new JMenu("save");
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int error = fileChooser.showOpenDialog(null);

            }
        });
        save.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
                fileChooser.setDialogTitle("save");
                fileChooser.setApproveButtonText("save");
                int error = fileChooser.showOpenDialog(null);

                try (FileWriter saving = new FileWriter(fileChooser.getSelectedFile()+"")) {
                    ArrayList<Shape> shapes = drawModel.shapes;           System.out.println(shapes.size()); System.out.println("shape data ");
                    saving.write(Integer.toString(shapes.size()));
                    for(int i=0;i<shapes.size();i++) {
                        System.out.println("shape data " );
                        String a = shapes.get(i).getDataString();

                        saving.write(shapes.get(i).getDataString());
                    }
                } catch (Exception ex) {
                    System.out.println("file save null exception");
                }

            }
        });



        JMenuItem load = new JMenu("load");
        load.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
                fileChooser.setDialogTitle("load");
                fileChooser.setApproveButtonText("load");
                int error = fileChooser.showOpenDialog(null);


                File file = fileChooser.getSelectedFile();

                try (Scanner sfile = new Scanner(fileChooser.getSelectedFile());) {
                    ArrayList<Shape> shapes = new ArrayList<Shape>();
                    shapes.clear();
                    int size = sfile.nextInt();             System.out.println(size); System.out.println("shape data ");
                    for(int i=0;i<size;i++) {
                        System.out.println("shape data " );
                        Point p = new Point(1,1);
                        Shape s = new Shape(p);
                        s.type = sfile.next();
                        s.p.x = sfile.nextInt();
                        s.p.y = sfile.nextInt();
                        s.p2.x = sfile.nextInt();
                        s.p2.y = sfile.nextInt();
                        s.width = sfile.nextInt();
                        s.height = sfile.nextInt();
                        s.thick = sfile.nextInt();
                        int colorRGB = sfile.nextInt();
                        if(colorRGB == 0) {
                            s.color = null;
                        } else {
                            s.color = new Color(colorRGB);
                        }

                        shapes.add(s);
                    }
                    drawModel.setShapes(shapes);
                } catch (Exception ex) {
                    System.out.println("file save null exception");
                }

            }
        });

        file.add(save);
        file.add(load);

        JMenuItem hasScrollBar = new JMenu("ScrollBar");
        JMenuItem noScrollBar = new JMenu("noScrollBar");
        menuBar.add(view);
        view.add(hasScrollBar);
        view.add(noScrollBar);

        Scrollbar horS = new Scrollbar(Scrollbar.HORIZONTAL);
        Scrollbar verS = new Scrollbar(Scrollbar.VERTICAL);


        hasScrollBar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                System.out.println("abvc");
                //mainPanel.remove(panelView);
                mainPanel.add(panelViewWrapper,BorderLayout.WEST);
//                mainPanel.add(verS,BorderLayout.EAST);
//                mainPanel.add(horS,BorderLayout.SOUTH);

                mainPanelS.setSize(new Dimension(800,620));
                mainPanelS.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

               frame.getContentPane().add(mainPanelS);frame.getContentPane().remove(mainPanel);

                mainPanelS.add(mainPanel);
                mainPanel.repaint();
            }
        });
        noScrollBar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                System.out.println("abvc");
                //mainPanel.remove(panelView);
                mainPanel.add(panelView,BorderLayout.WEST);

                mainPanel.remove(horS);
                mainPanel.remove(verS);
                mainPanel.repaint();
            }
        });




        //mainPanel.add(panelViewWrapper,BorderLayout.WEST);
        mainPanel.add(panelView,BorderLayout.WEST);
        mainPanel.add(drawPanelOut,BorderLayout.EAST);
        frame.add(menuBar,BorderLayout.NORTH);
        frame.setVisible(true);

    }

}
